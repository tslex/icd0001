import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

    public static void main(String[] param) {


        // conversion double -> String

        double someDouble = 1.0;
        String stringFromDouble = Double.toString(someDouble);
        System.out.println(stringFromDouble);

        // conversion String -> int

        String numericString = "867443";
//        String numericString = "86744a";
        try{
            int someInt = Integer.valueOf(numericString);
            System.out.println(someInt);
        }
        catch (NumberFormatException ex){
            System.err.println("String is not numeric!");
        }

        // "hh:mm:ss"

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        System.out.println( sdf.format(cal.getTime()) );

        // cos 45 deg

        System.out.println("Cos 45deg is: " + Math.cos(Math.toRadians(45)));

        // table of square roots

        for (double i = 0; i <= 100; i+=5) {
            System.out.println("Square root for " + i + " is " + Math.sqrt(i));
        }

        String firstString = "ABcd12";
        String result = reverseCase(firstString);
        System.out.println("\"" + firstString + "\" -> \"" + result + "\"");

        // reverse string

        String normalString = "1234ab";
        StringBuilder reversedString = new StringBuilder();
        char[] tmp = normalString.toCharArray();
        for (int i = tmp.length - 1; i >= 0; i--) {
            reversedString.append(tmp[i]);
        }
        System.out.println(reversedString.toString());


        String s = "How  many	 words   here";
        int nw = countWords(s);
        System.out.println(s + "\t" + String.valueOf(nw));

        // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!

        long currentTime = System.currentTimeMillis();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Real delay was " + (System.currentTimeMillis() - currentTime));

        final int LIST_SIZE = 100;
        ArrayList<Integer> randList = new ArrayList<>(LIST_SIZE);

        Random generator = new Random();
        for (int i = 0; i < LIST_SIZE; i++) {
            randList.add(generator.nextInt(1000));
        }
        System.out.println(randList);

        // minimal element
        Integer min = randList.get(0);
        for (Integer integer : randList) {
            if (integer < min) min = integer;
        }
        System.out.println(min);

        // HashMap tasks:
        //    create
        HashMap<String, String> map = new HashMap<>();
        map.put("ICD0001", "SAMPLE TEXT HERE");
        map.put("ICD0002", "SAMPLE TEXT HERE");
        map.put("ICD000...", "SAMPLE TEXT HERE");
        map.put("ICD0006", "SAMPLE TEXT HERE");
        map.put("ICD0007", "SAMPLE TEXT HERE");
        //    print all keys
        System.out.println(map.keySet());
        //    remove a key
        map.remove("ICD0007");
        System.out.println(map.containsKey("ICD0007"));
        //    print all pairs
        for (String key : map.keySet()) {
            System.out.println("key is: " + key + ", with value: " + map.get(key));
        }

        System.out.println("Before reverse:  " + randList);
        reverseList(randList);
        System.out.println("After reverse: " + randList);

        System.out.println("Maximum: " + maximum(randList));
    }

    /**
     * Finding the maximal element.
     *
     * @param a Collection of Comparable elements
     * @return maximal element.
     * @throws NoSuchElementException if <code> a </code> is empty.
     */
    static public <T extends Object & Comparable<? super T>>
    T maximum(Collection<? extends T> a)
            throws NoSuchElementException {

        T[] temp = (T[]) a.toArray();

        if (a.size() < 2) return temp[0];

        T max = temp[0];

        for (int i = 1; i < temp.length; i++) {
            if (temp[i].compareTo(max) > 0) max = temp[i];
        }

        return max;
    }

    /**
     * Counting the number of words. Any number of any kind of
     * whitespace symbols between words is allowed.
     *
     * @param text text
     * @return number of words in the text
     */
    public static int countWords(String text) {
        
//        int count = 0;
//        boolean isWordBegin = false;
//
//        for (Character ch: text.toCharArray()) {
//            if (!Character.isAlphabetic(ch) && isWordBegin){
//                isWordBegin = false;
//            }
//            else if (Character.isAlphabetic(ch) && !isWordBegin){
//                isWordBegin = true;
//                count++;
//            }
//        }
//
//        return count;
        return text.split("(?<=\\w)\\s+(?=\\w)").length;
    }

    /**
     * Case-reverse. Upper -> lower AND lower -> upper.
     *
     * @param s string
     * @return processed string
     */

    public static String reverseCase(String s) {

        StringBuilder reversed = new StringBuilder();
        for (Character ch : s.toCharArray()) {
            reversed.append(Character.isUpperCase(ch)
                    ? Character.toLowerCase(ch) : Character.toUpperCase(ch));
        }
        return reversed.toString();

    }

    /**
     * List reverse. Do not create a new list.
     *
     * @param list list to reverse
     */
    public static <T extends Object> void reverseList(List<T> list)
            throws UnsupportedOperationException {

        if (list.size() < 2) return;

        int leftIndex = 0;
        int rightIndex = list.size() - 1;

        T tmp;

        do {

            tmp = list.get(leftIndex);
            list.set(leftIndex, list.get(rightIndex));
            list.set(rightIndex, tmp);

            leftIndex++;
            rightIndex--;

        } while (leftIndex < rightIndex);

    }
}
