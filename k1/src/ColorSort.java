public class ColorSort {

    enum Color {red, green, blue}

    public static void main(String[] param) {
        // for debugging
    }

    /* Fixing
     * add comments
     *
     * speed improvement seems to be maximum of this algorithm
     * */

    public static void reorder(Color[] balls) {

        Color[] unsorted = balls.clone();

        //counter for balls
        int rCount = 0;
        int gCount = 0;

        for (Color color : unsorted) {
            switch (color) {
                case red:
                    rCount++;
                    break;
                case green:
                    gCount++;
                    break;
            }
        }

        int redIndex = 0; //reds starts allays at zero
        int greenIndex = rCount; //green start after last red ball
        int blueIndex = rCount + gCount; //blue start after last green (which starts after last red)

        // sorting...
        for (Color color : unsorted) {

            // switching colors and applying index++ after placement for next ball
            switch (color) {
                case red:
                    balls[redIndex] = color;
                    redIndex++;
                    break;
                case green:
                    balls[greenIndex] = color;
                    greenIndex++;
                    break;
                case blue:
                    balls[blueIndex] = color;
                    blueIndex++;
                    break;
            }
        }
    }
}

