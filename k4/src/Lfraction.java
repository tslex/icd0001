import java.util.Arrays;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {

//        Lfraction a = new Lfraction(4, 20);
        Lfraction b = Lfraction.valueOf("4/-8/");
//        Lfraction b = new Lfraction(2, 10);

//        Lfraction c = a.plus(b);
        System.out.println(b);
    }

    /**
     * Instance variables
     */
    private long numerator;  //  числитель (сверху)
    private long denominator; // значениталь (снизу)

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) throw new RuntimeException(
                "Fraction ( " + a + ", " + b + " ) cannot be created -> denominator cannot be zero"
        );

        this.numerator = a;
        this.denominator = b;
        formatFraction();
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        formatFraction();
        return numerator + "/" + denominator;
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
//        return (m instanceof Lfraction &&
//                numerator == ((Lfraction) m).getNumerator() &&
//                denominator == ((Lfraction) m).getDenominator());
        return compareTo((Lfraction) m) == 0;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        formatFraction();
//        return (int) Math.pow(
//                numerator % Integer.MAX_VALUE, denominator % Integer.MAX_VALUE)
//                % Integer.MAX_VALUE;
//        return Arrays.hashCode(toString().getBytes());

        double hash = 0;

        int post = 0;

        long num = numerator;
        long den = denominator;

        if (num == 1){
            num++;
            post--;
        }

        hash = Math.pow(num % 999, den & 999);
        return (int) hash + post;
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {

        formatFraction();
        m.formatFraction();

        long commonDenominator = getCommonDenomitator(denominator, m.getDenominator());

        long numeratorA = numerator * commonDenominator / denominator;
        long numeratorB = m.getNumerator() * commonDenominator / m.getDenominator();

        return new Lfraction(numeratorA + numeratorB, commonDenominator);
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        return new Lfraction(numerator * m.numerator, denominator * m.denominator);
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (numerator == 0) throw new RuntimeException(
                "Fraction ( " + numerator + ", " + denominator + " ) cannot be inversed -> numerator cannot be zero"
        );
        return new Lfraction(denominator, numerator);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-1 * numerator, denominator);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        return plus(m.opposite());
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (m.numerator == 0) throw new RuntimeException(
                "Fraction" + this.toString() + "cannot be divided by " + m.toString() + " -> will cause division by zero"
        );
        return times(m.inverse());
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        return Long.compare(numerator * m.denominator, denominator * m.numerator);
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(numerator, denominator);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return numerator / denominator;
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        return new Lfraction(numerator - integerPart() * denominator, denominator);
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) numerator / (double) denominator;
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        return new Lfraction(Math.round(f * d), d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {

        long numerator;
        long denominator;

        String input = s.trim();

        if (input.equals("")) return null;

        String[] fractionParts = input.replace("/", "/ ").split("/");

        if (fractionParts.length > 2) {
            throw new RuntimeException(
                    s + " -> Wrong fraction format. Should be \"m/n\" or \"m\""
            );
        } else if (fractionParts.length == 2) {

            try {
                numerator = Long.valueOf(fractionParts[0].trim());
            } catch (Exception ex) {
                throw new RuntimeException(
                        s + " -> \"" + fractionParts[0] + "\" - non-integer value"
                );
            }
            try {
                denominator = Long.valueOf(fractionParts[1].trim());
            } catch (Exception ex) {
                throw new RuntimeException(
                        s + " -> \"" + fractionParts[1] + "\" - non-integer value"
                );
            }
        } else {

            denominator = 1;

            try {
                numerator = Long.valueOf(fractionParts[0].trim());
            } catch (Exception ex) {
                throw new RuntimeException(
                        s + " -> \"" + fractionParts[0] + "\" - non-integer value"
                );
            }
        }

        return new Lfraction(numerator, denominator);
    }

    /**
     * Format fraction denominator to be positive and reduce fraction
     */
    public void formatFraction() {
        long divisor = getGCD();

        this.numerator /= divisor;
        this.denominator /= divisor;

        if (denominator < 0) {
            this.numerator *= -1;
            this.denominator *= -1;
        }

        if (numerator == 0) {
            this.denominator = 1;
        }
    }

    /**
     * Get greatest common divisor
     *
     * @return greatest common divisor
     */
    private long getGCD() {
        long divisor = 1;

        for (long i = 1; i <= Math.abs(numerator) && i <= Math.abs(denominator); i++) {
            if (numerator % i == 0 && denominator % i == 0) divisor = i;
        }

        return divisor;
    }

    /**
     * Get common denominator
     *
     * @param a first denominator
     * @param b second denominator
     * @return common denominator (non-effective way)
     */
    public static long getCommonDenomitator(long a, long b) {
        return a * b;
    }

    /**
     * Multiply fraction by integer
     *
     * @param a multiplier
     * @deprecated
     */

    public void multiply(long a) {
        this.numerator *= a;
        this.denominator *= a;
    }

    /**
     * Convert fraction to floating point fraction
     *
     * @deprecated
     */
    public double convertToDouble() {
        return ((double) numerator) / (double) denominator;
    }
}


