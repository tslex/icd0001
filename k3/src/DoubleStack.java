import com.sun.istack.internal.NotNull;

import java.util.LinkedList;

public class DoubleStack {

    public static void main(String[] argum) {
        System.out.println(interpret(""));
    }

    private LinkedList<Double> stack;

    DoubleStack() {
        this.stack = new LinkedList<>();
    }

    private DoubleStack(LinkedList<Double> stack) {
        this.stack = stack;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new DoubleStack((LinkedList<Double>) stack.clone());
    }

    public boolean stEmpty() {
        return stack.size() == 0;
    }

    public void push(double a) {
        stack.addLast(a);
    }

    public double pop() {
        if (stEmpty()) throw new RuntimeException("There are no elements inside");
        return stack.removeLast();
    }

    public void op(String s) {

        if (size() < 2)
            throw new RuntimeException("Not enough operands: " + toString() + ", for operation: " + s);

        Double last = pop();
        Double prelast = pop();
        Double result;

        switch (s) {
            case "+":
                result = prelast + last;
                break;
            case "-":
                result = prelast - last;
                break;
            case "*":
                result = prelast * last;
                break;
            case "/":
                result = prelast / last;
                break;
            default:
                throw new RuntimeException("Invalid/unsupported operation: " + s);
        }

        push(result);
    }

    public double tos() {
        if (stEmpty()) throw new RuntimeException("There are no elements inside");
        return stack.getLast();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DoubleStack)) return false;

        DoubleStack comp = (DoubleStack) o;

        if (comp.size() != size()) return false;

        for (int i = 0; i < stack.size(); i++) {
            if (!stack.get(i).equals(comp.get(i))) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return stack.toString();
    }

    public int size() {
        return stack.size();
    }

    public double get(int index) {
        return stack.get(index);
    }

    // new methods
    public void swap() {

        if (size() < 2)
            throw new RuntimeException("Not enough operands: " + toString() + ", for operation: SWAP, must be at least two");

        double last = pop();
        double prelast = pop();

        push(last);
        push(prelast);
    }

    public void rot() {

        if (size() < 3)
            throw new RuntimeException("Not enough operands: " + toString() + ", for operation: ROT, must be at least three");

        double last = pop();            //c
        double prelast = pop();         //b
        double preprelast = pop();      //a

        push(prelast);
        push(last);
        push(preprelast);
    }

    public static double interpret(String pol) {

        if (pol == null) {
            throw new RuntimeException(genErrMsg(pol, "Entered null as argument"));
        }

        String[] input = pol.trim().split("\\s+");
        DoubleStack doubleStack = new DoubleStack();

        int index = 0;

        for (String s : input) {

            if (s.equals("")) {
                throw new RuntimeException(genErrMsg(pol, "String with index " + index + " is empty"));
            }

            try {

                if (s.toUpperCase().equals("SWAP")) {
                    doubleStack.swap();
                    continue;
                }

                if (s.toUpperCase().equals("ROT")) {
                    doubleStack.rot();
                    continue;
                }

                switch (s) {
                    case "+":
                    case "-":
                    case "/":
                    case "*":
                        doubleStack.op(s);
                        break;
                    default:
                        doubleStack.push(Double.valueOf(s.trim()));
                        break;
                }
            } catch (NumberFormatException ex) {
                throw new RuntimeException(
                        genErrMsg(pol, "\"" + s + "\" is not a double or correct operation symbol"));
            } catch (RuntimeException ex) {
                throw new RuntimeException(
                        genErrMsg(pol, ex.getMessage()));
            }

            index++;
        }

        if (doubleStack.size() != 1)
            throw new RuntimeException(
                    genErrMsg(pol, "No operation left, but there are more than one elements inside: " + doubleStack.toString()));
        return doubleStack.tos();
    }

    private static String genErrMsg(String pol, String msg) {

        // Wrap message with input string "pol"

        return "Wrong equation: \"" + pol + "\"\nReason: " + msg;
    }
}

