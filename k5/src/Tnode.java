import java.util.Stack;

public class Tnode {

    private String name;
    private Tnode firstChild = null;
    private Tnode nextSibling = null;

    @Override
    public String toString() {

        StringBuilder b = new StringBuilder();

        b.append(name);

        if (firstChild != null){
            b.append("(");
            b.append(firstChild.toString());
            b.append(")");
        }

        if (nextSibling != null){
            b.append(",");
            b.append(nextSibling.toString());
        }

        return b.toString();
    }

    public Tnode(String name) {
        this.name = name;
    }

    public static Tnode buildFromRPN(String pol) {

        Stack<Tnode> stack = new Stack<>();
        String[] raw = pol.split(" ");

        Tnode root, child1, child2, child3;

        if (raw.length < 1) throw new RuntimeException(
                "argument: " + "\"" + pol + "\"" + " -> string is empty"
        );

        for (String s : raw) {

            if (s.toUpperCase().trim().equals("SWAP")){
                if (stack.size() < 2)
                    throw new RuntimeException(
                            "\"" + pol + "\"" + " -> " + "not enough items for SWAP, must be at lest 2"
                    );

                child1 = stack.pop();
                child2 = stack.pop();

                stack.push(child1);
                stack.push(child2);
            }
            else if (s.toUpperCase().trim().equals("ROT")){
                if (stack.size() < 3)
                    throw new RuntimeException(
                            "\"" + pol + "\"" + " -> " + "not enough items for ROT, must be at lest 3"
                    );

                child1 = stack.pop();
                child2 = stack.pop();
                child3 = stack.pop();

                stack.push(child2);
                stack.push(child1);
                stack.push(child3);
            }
            else if (isOperation(s)) {
                if (stack.size() < 2){
                    throw new RuntimeException(
                            "\"" + pol + "\"" + " -> " +
                                    "not enough children for parent node: " + "\"" + s + "\""
                    );
                }

                child2 = stack.pop();
                child1 = stack.pop();
                child1.nextSibling = child2;

                root = new Tnode(s);
                root.firstChild = child1;
                stack.push(root);

            } else {
                if (!isNumeric(s)) {
                    throw new RuntimeException(
                            "\"" + pol + "\"" + " -> " +
                                    "node must be digit or [+-/*], " +
                                    "but was: " + "\"" + s + "\""
                    );
                }

                stack.push(new Tnode(s));
            }
        }

        root = stack.pop();

        if (!stack.empty()){
            throw new RuntimeException(
                    "\"" + pol + "\"" + " -> at the end must be only one node, but was: " + (stack.size() + 1)
            );
        }

        return root;
    }

    public static void main(String[] param) {
//      String rpn = "1 2 +";
//      String rpn = "5 1 - 7 * 6 3 / +";
//      String rpn = "x";
//      String rpn = "+";


      String rpn = "2 5 9 ROT / /";


      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
    }

    public static boolean isOperation(String s) {
        return s.matches("[+\\-/*]");
    }

    public static boolean isNumeric(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

