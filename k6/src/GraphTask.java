import java.util.*;

public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Task tasks = new Task();

//        tasks.simpleTest1(); // - aeg 0.002s
//        tasks.simpleTest2(); // - aeg 0.003s
//        tasks.simpleTest3(); // - aeg 0.001s
//
//        tasks.randomTest1(); // - aeg 0.001s
//        tasks.randomTest2(); // - aeg 0.008s

//        tasks.hugeGraph1(); // - aeg 0.038s
//        tasks.hugeGraph2(); // - väga palju aega - ei saa tulemust saada
//        tasks.hugeGraph3(); // - väga palju aega - ei saa tulemust saada
    }

    /**
     * Vertex represents a node in graph.
     * Method findMostFarVertex is recursive. His purpose to find the *deepest* Vertex with max weight sum.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        /**
         * Constructor.
         *
         * @param s - Vertex id.
         * @param v - Next Vertex.
         */
        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        /**
         * Constructor.
         *
         * @param s - Vertex id.
         */
        Vertex(String s) {
            this(s, null, null);
        }

        /**
         * Return string representation of Vertex.
         */
        @Override
        public String toString() {
            return id;
        }

        /**
         * Find the most far Vertex object.
         *
         * @param struct Object of class TheMostFarVertex. Data storage.
         * @return Data storage with the result and other useful information.
         */
        public TheMostFarVertex findMostFarVertex(TheMostFarVertex struct) {

            struct.passedVertices.add(this);
            List<TheMostFarVertex> results = new ArrayList<>();

            Arc a = first;
            while (a != null) {


                if (!struct.passedVertices.contains(a.target)) {

                    TheMostFarVertex newStruct = struct.clone();
                    newStruct.spotVertex = this;
                    newStruct.farVertex = a.target;
                    newStruct.iteration++;
                    newStruct.distance += a.weight;
                    newStruct.updateHistory();

                    results.add(a.target.findMostFarVertex(newStruct));
                }

                a = a.next;
            }

            if (results.size() > 1) {
                return Collections.max(results);
            } else if (results.size() == 1) {
                return results.get(0);
            } else return struct;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        private int weight = 0; // represents arc weight.

        /**
         * Constructor.
         *
         * @param s - Arc id.
         * @param v - Target Vertex.
         * @param a - Next Arc.
         */
        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Constructor.
         *
         * @param s - Arc id.
         * @param v - Target Vertex.
         * @param a - Next Arc.
         * @param w - Weight. May be initialized in constructor.
         */
        Arc(String s, Vertex v, Arc a, int w) {
            id = s;
            target = v;
            next = a;
            weight = w;
        }

        /**
         * Constructor.
         *
         * @param s - Arc id.
         */
        Arc(String s) {
            this(s, null, null);
        }

        /**
         * Return string representation of Arc.
         */
        @Override
        public String toString() {
            return id + "(" + weight + ")";
        }

        /**
         * Set weight to Arc with validation.
         *
         * @param weight - must be >= 0.
         */
        public void setWeight(int weight) {
            if (weight < 0) {
                throw new RuntimeException("\"" + weight + "\" -> Arc weight must be equal or bigger the 0 (>=)");
            }
            this.weight = weight;
        }
    }

    /**
     * Graph represents graph.
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        // Map of Arc's weight. Each record is for Two-directional edges.
        private Map<Set<Vertex>, Arc> arcWeightMap = new HashMap<>();

        private Random randomWeight = new Random();

        private Vertex[] vert;
        int[][] connected;
        int vertexCount;
        int edgesCount;

        /**
         * Constructor.
         *
         * @param s - Graph id.
         * @param v - First Vertex.
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        /**
         * Constructor.
         *
         * @param s - Graph id.
         */
        Graph(String s) {
            this(s, null);
        }

        /**
         * Return string representation of Arc.
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Create new Vertex with id.
         *
         * @param vid - New Vertex's Id
         * @return Vertex
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Create new Arc with id, source Vertex and target Vertex.
         *
         * @param aid  - New Arc's id.
         * @param from - Source Vertex.
         * @param to   - Target Vertex.
         * @return Arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);

            vertexCount = n;
            edgesCount = m;

            first = null;
            createRandomTree(n);       // n-1 edges created here

            vert = new Vertex[n];


            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }

            connected = createAdjMatrix();


            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        //===>

        /**
         * Create an empty graph with n count of Vertexes. No Arcs.
         *
         * @param n - number of Vertexes
         */
        public void createEmptyGraph(int n) {
            vertexCount = n;
            edgesCount = 0;

            vert = new Vertex[n];

            for (int i = 0; i < n; i++) {
                vert[i] = new Vertex("v" + (i + 1));
                vert[i].info = i;
                if (i != 0) {
                    vert[i - 1].next = vert[i];
                }
            }

            first = vert[0];

            connected = new int[n][n];
        }

        /**
         * Set random weight to all edges - pairs of Arts (two directions).
         *
         * @param bound - max of random value (0 <= x <= bound).
         */
        public void setRandomArcWeight(int bound) {
            Vertex v = first;

            while (v != null) {

                Arc a = v.first;

                while (a != null) {

                    Set<Vertex> vertexSet = new HashSet<>();
                    vertexSet.add(v);
                    vertexSet.add(a.target);

                    if (!arcWeightMap.containsKey(vertexSet)) {
                        a.setWeight(randomWeight.nextInt(bound));
                        arcWeightMap.put(vertexSet, a);
                    } else {
                        a.setWeight(arcWeightMap.get(vertexSet).weight);
                    }

                    a = a.next;
                }

                v = v.next;
            }

            arcWeightMap = new HashMap<>();
        }

        /**
         * Begin search of the most far Vertex. Call recursive function 'findMostFarVertex' is source Vertex.
         *
         * @param sourceVertex - Source Vertex
         * @return Data storage with the result and other useful information.
         */
        public TheMostFarVertex getTheMostFarVertex(Vertex sourceVertex) {

            TheMostFarVertex struct = new TheMostFarVertex();
            struct.sourceVertex = sourceVertex;
            struct.spotVertex = sourceVertex;
            struct.updateHistory();

            return sourceVertex.findMostFarVertex(struct);
        }
    }

    /**
     * TheMostFarVertex represent data storage for most far Vertex search.
     */
    class TheMostFarVertex implements Comparable {

        private Vertex sourceVertex;
        private Vertex spotVertex;
        private Vertex farVertex;

        private int distance = 0;
        private int iteration = 0;

        private Set<Vertex> passedVertices = new HashSet<>();
        private List<String> moveHistory = new ArrayList<>();

        /**
         * Updates search history.
         * On each iteration it can see: from which Vertex, to which Vertex
         * and total weight of passed path.
         */
        private void updateHistory() {
            StringBuilder string = new StringBuilder();

            string.append(iteration);
            string.append(": ");
            string.append(spotVertex);
            if (iteration > 0) {
                string.append(" ==> ");
                string.append(farVertex);
                string.append(" (");
                string.append(distance);
                string.append(")");
            }

            moveHistory.add(string.toString());
        }

        /**
         * Makes clone, for new recursive call.
         *
         * @return Clone of this object.
         */
        public TheMostFarVertex clone() {
            TheMostFarVertex clone = new TheMostFarVertex();
            clone.sourceVertex = sourceVertex;
            clone.spotVertex = spotVertex;
            clone.farVertex = farVertex;

            clone.distance = distance;
            clone.iteration = iteration;

            clone.passedVertices = new HashSet<>(passedVertices);
            clone.moveHistory = new ArrayList<>(moveHistory);

            return clone;
        }

        /**
         * Compare this with other TheMostFarVertex object.
         *
         * @param o - TheMostFarVertex object.
         * @return comparation result (-1, 0, 1).
         */
        @Override
        public int compareTo(Object o) {
            TheMostFarVertex comp = (TheMostFarVertex) o;

            return Integer.compare(distance, comp.distance);
        }

        /**
         * Return string representation of Arc.
         */
        @Override
        public String toString() {
            return
                    getDistance() +
                            "\n" +
                            "============================" +
                            "\n" +
                            getMostFar() +
                            "\n" +
                            "============================" +
                            "\n" +
                            getHistory();
        }

        /**
         * Return current history.
         */
        public String getHistory() {
            return String.join("\n", moveHistory);
        }

        /**
         * Return current distance.
         */
        public String getDistance() {
            return "Distance: " + distance;
        }

        /**
         * Return current most far Vertex made so far.
         */
        public String getMostFar() {
            return "The Most Far: " + farVertex;
        }
    }

    /**
     * Consist of different tests to demonstrate work of most far vertex finding algorithm
     */
    class Task {

        /**
         * Create and test custom graph #1.
         */
        public void simpleTest1() {

            Graph g = new Graph("Graph with 8 vertexes and 7 edges");


            System.out.println("Creating graph...");

            g.createEmptyGraph(8);

            g.vert[0].first = new Arc("av1_v2", g.vert[1], new Arc("av1_v4", g.vert[3], new Arc("av1_v6", g.vert[5], null, 11), 34), 45); //1
            g.vert[1].first = new Arc("av2_v1", g.vert[0], new Arc("av2_v3", g.vert[2], null, 13), 45); //2
            g.vert[2].first = new Arc("av3_v2", g.vert[1], null, 13); //3
            g.vert[3].first = new Arc("av4_v1", g.vert[0], new Arc("av4_v5", g.vert[4], null, 46), 34); //4
            g.vert[4].first = new Arc("av5_v4", g.vert[3], null, 46); //5
            g.vert[5].first = new Arc("av6_v1", g.vert[0], new Arc("av6_v7", g.vert[6], null, 23), 11); //6
            g.vert[6].first = new Arc("av7_v6", g.vert[5], new Arc("av7_v8", g.vert[7], null, 99), 23); //7
            g.vert[7].first = new Arc("av8_v7", g.vert[6], null, 99); //5

            System.out.println(g);

            System.out.println("Finding most far vertex");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test custom graph #2.
         */
        public void simpleTest2() {
            Graph g = new Graph("Graph with 6 vertexes and 6 edges");


            System.out.println("Creating graph...");

            g.createEmptyGraph(6);

            g.vert[0].first = new Arc("av1_v2", g.vert[1], null, 11); //1
            g.vert[1].first = new Arc("av2_v1", g.vert[0], new Arc("av2_v3", g.vert[2], new Arc("av2_v4", g.vert[3], new Arc("av2_v6", g.vert[5], null, 99), 23), 51), 11); //2
            g.vert[2].first = new Arc("av3_v2", g.vert[1], new Arc("av3_v4", g.vert[3], null, 32), 51); //3
            g.vert[3].first = new Arc("av4_v2", g.vert[1], new Arc("av4_v3", g.vert[2], new Arc("av4_v5", g.vert[4], null, 22), 32), 23); //4
            g.vert[4].first = new Arc("av5_v4", g.vert[3], null, 22); //5
            g.vert[5].first = new Arc("av6_v2", g.vert[1], null, 99); //6

            System.out.println(g);

            System.out.println("Finding most far vertex");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test custom graph #3.
         */
        public void simpleTest3() {
            Graph g = new Graph("Graph with 4 vertexes and 5 edges");


            System.out.println("Creating graph...");

            g.createEmptyGraph(4);

            g.vert[0].first = new Arc("av1_v2", g.vert[1],
                    new Arc("av1_v3", g.vert[2], null, 67), 30);

            g.vert[1].first = new Arc("av2_v1", g.vert[0],
                    new Arc("av2_v3", g.vert[2],
                            new Arc("av2_v4", g.vert[3], null, 10), 39), 30);

            g.vert[2].first = new Arc("av3_v1", g.vert[0],
                    new Arc("av3_v2", g.vert[1],
                            new Arc("av3_v4", g.vert[3], null, 76), 39), 67);

            g.vert[3].first = new Arc("av4_v2", g.vert[1],
                    new Arc("av4_v3", g.vert[2], null, 76), 10);

            System.out.println(g);

            System.out.println("Finding most far vertex");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test random graph #1.
         */
        public void randomTest1() {
            Graph g = new Graph("Graph with 6 vertexes and 9 edges");

            System.out.println("Creating graph...");
            g.createRandomSimpleGraph(6, 9);


            System.out.println("Calculating weight...");
            g.setRandomArcWeight(100);

            System.out.println(g);

            System.out.println("Finding most far vertex...");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test random graph #2.
         */
        public void randomTest2() {
            Graph g = new Graph("Graph with 16 vertexes and 19 edges");

            System.out.println("Creating graph...");
            g.createRandomSimpleGraph(16, 19);


            System.out.println("Calculating weight...");
            g.setRandomArcWeight(100);

            System.out.println(g);

            System.out.println("Finding most far vertex...");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test huge graph (2000 vertexes and 2000 edges) #1.
         */
        public void hugeGraph1() {
            Graph g = new Graph("Graph with 2000 vertexes and 2000 edges");

            System.out.println("Creating graph...");
            g.createRandomSimpleGraph(2000, 2000);


            System.out.println("Calculating weight...");
            g.setRandomArcWeight(100);

            System.out.println(g);

            System.out.println("Finding most far vertex...");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        /**
         * Create and test huge graph (2000 vertexes and 2050 edges) #2.
         */
        public void hugeGraph2() {
            Graph g = new Graph("Graph with 2000 vertexes and 2050 edges");

            System.out.println("Creating graph...");
            g.createRandomSimpleGraph(2000, 2050);


            System.out.println("Calculating weight...");
            g.setRandomArcWeight(100);

            System.out.println(g);

            System.out.println("Finding most far vertex...");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }

        public void hugeGraph3() {
            Graph g = new Graph("Graph with 2000 vertexes and 3000 edges");

            System.out.println("Creating graph...");
            g.createRandomSimpleGraph(2000, 3000);


            System.out.println("Calculating weight...");
            g.setRandomArcWeight(100);

            System.out.println(g);

            System.out.println("Finding most far vertex...");

            long currTime = System.currentTimeMillis();

            TheMostFarVertex mostFarVertex = g.getTheMostFarVertex(g.first);

            long spendTime = System.currentTimeMillis() - currTime;
            System.out.println("Found");
            System.out.println(mostFarVertex);
            System.out.println("Time elapsed: " + (spendTime / 1000D) + "s.");
        }
    }
}
