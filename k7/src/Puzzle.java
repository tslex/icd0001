import java.util.*;

public class Puzzle {

    static Character[] symbols;
    static List<Map<Character, Integer>> solutions;

    static Pattern addend1;
    static Pattern addend2;
    static Pattern sum;

    /**
     * puzzle the word puzzle.
     *
     * @param args three words (addend1 addend2 sum)
     */
    public static void main(String[] args) {
        puzzle(args);       //solve
        printSolution();    //print result
    }

    /**
     * Method for preparation.
     *
     * @param args three words (addend1 addend2 sum)
     */
    private static void puzzle(String[] args){
        if (args.length < 3) {
            throw new RuntimeException(
                    "\"" + Arrays.toString(args) + "\" -> all three args must be set");
        }
        if (Math.max(args[0].length(), args[1].length()) > args[2].length()) {
            throw new RuntimeException(
                    "\"" + Arrays.toString(args) + "\" -> " +
                            "addends lengths must not be bigger than sum length");
        }
        if (Math.min(Math.min(args[0].length(), args[1].length()), args[2].length()) <= 0) {
            throw new RuntimeException(
                    "\"" + Arrays.toString(args) + "\" -> " +
                            "arguments cannot be empty");
        }
        if (args[0].matches("[^A-Za-z]+") ||
                args[1].matches("[^A-Za-z]+") ||
                args[2].matches("[^A-Za-z]+")) {
            throw new RuntimeException(
                    "\"" + Arrays.toString(args) + "\" -> " +
                            "arguments may just consist out of letters");
        }

        Set<Character> tmp = new HashSet<>();

        addend1 = new Pattern(args[0].toUpperCase());
        addend2 = new Pattern(args[1].toUpperCase());
        sum = new Pattern(args[2].toUpperCase());

        for (char ch : args[0].toCharArray()) {
            tmp.add(ch);
        }
        for (char ch : args[1].toCharArray()) {
            tmp.add(ch);
        }
        for (char ch : args[2].toCharArray()) {
            tmp.add(ch);
        }

        symbols = new Character[tmp.size()];
        tmp.toArray(symbols);

        if (symbols.length > 10) {
            throw new RuntimeException(
                    "\"" + Arrays.toString(args) + "\" -> " +
                            "cannot assign unique digit to each symbol");
        }

//        System.out.println(Arrays.toString(symbols));

        solutions = solve(0, null);
    }

    /**
     * Recursive method for calculating of solution.
     *
     * @param index current symbol index
     * @param solution current solution
     * @return list of solutions
     */
    private static List<Map<Character, Integer>> solve(Integer index, Map<Character, Integer> solution) {

        List<Map<Character, Integer>> solutions = new ArrayList<>();
        Map<Character, Integer> newSolution;


        for (int i = 0; i < 10; i++) {

            if (solution == null) {
                newSolution = new HashMap<>();
            } else if (solution.containsValue(i)) {
                continue;
            } else {
                newSolution = new HashMap<>(solution);
            }

            newSolution.put(symbols[index], i);

//            System.out.println(newSolution.toString());

            if (index == symbols.length - 1) {
                if (addend1.constructFromSolution(newSolution) + addend2.constructFromSolution(newSolution)
                        == sum.constructFromSolution(newSolution)) {
                    solutions.add(newSolution);
                }
            } else {
                solutions.addAll(solve(index + 1, newSolution));
            }
        }

        return solutions;
    }

    /**
     * Method for getting the result of puzzle.
     */
    public static void printSolution(){
        System.out.println("  " + addend1.toString());
        System.out.println("+ " + addend2.toString());
        System.out.println("  =========");
        System.out.println("  " + sum.toString());
        System.out.println();
        System.out.println("Solution count: " + solutions.size());
        System.out.println();

        if (solutions.size() < 4) {
            System.out.println("Solutions: ");
            for (Map<Character, Integer> solution : solutions) {
                System.out.println(solution.toString());
            }
        }
    }

    /**
     * Class representing a word.
     */
    static class Pattern {

        private String value;

        public Pattern(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public long constructFromSolution(Map<Character, Integer> solution) {
            long sum = 0;

            for (int i = 0; i < value.length(); i++) {
                try {
                    long tmp = (long) (solution.get(value.charAt(i)) * Math.pow(10, (value.length() - 1 - i)));
                    if (i == 0 && tmp == 0) return -1;
                    sum += tmp;
                } catch (NullPointerException ex) {
                    return -1;
                }
            }

//            if (solution.get('A') == 1 &&
//                    solution.get('B') == 2 &&
//                    solution.get('I') == 9 &&
//                    solution.get('J') == 0
//            ) {
//                System.out.println(solution);
//                System.out.println(sum);
//            }
            return sum;
        }
    }
}

